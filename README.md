# PlagCheck

## Usage

### 0. Export Gitlab.crio.do Access Token
```bash
export OAUTH2=<ACCESS_TOKEN>
```
### 1. Download User/Stub Repositories
```
python ./repo_search/repo_downloader.py github "qkart automation"
```
### 2. GitHub/GitLab Search and Download Public Repos
```
python ./repo_search/repo_downloader.py gitlab_crio "COHORT_ME_QA_XFLIX_ENROLL_1714453952160"
```
### 3. Sanitize Repositories
```
python ./sanitize.py public_repos/ "*.java" sanitized_output/public_repos

python ./sanitize.py user_repos/ "*.java" sanitized_output/user_repos
```

### 4.1 Plagiarism Check using JPlag

```
python plagcheck.py -l java -i sanitized_output/user_repos/COHORT_ME_QA_XFLIX_ENROLL_1714453952160/ -o sanitized_output/public_repos/
```

### 4.2 If Checking Plagiarism for a single sanitized file
```
`python plagcheck.py -l java -i sanitized_output/user_repos/COHORT_ME_QA_XFLIX_ENROLL_1714453952160/ -o sanitized_output/public_repos/` -u sanitized_output/user_repos/COHORT_ME_QA_XFLIX_ENROLL_1714453952160/COHORT_ME_QA_XFLIX_ENROLL_1714453952160:abhishekmv2910-ME_QA_XFLIX.java
```
OR Run All Together (stubs, users is optional)
```
export OAUTH2=<CRIO_GITLAB_ACCESS_TOKEN>
./run.py -c "COHORT_ME_QA_XFLIX_ENROLL_1714453952160" -l java -q "xflix qa" #for all users in the cohort

#for just this user against the cohort and public repos
./run.py --cohort "COHORT_ME_QA_XFLIX_ENROLL_1714453952160" --language java --query "xflix qa" --users https://gitlab.crio.do/COHORT_ME_QA_XFLIX_ENROLL_1714453952160/abhishekmv2910-ME_QA_XFLIX --debug

./run.py -c "COHORT_ME_QA_XFLIX_ENROLL_1714453952160" -l java -q "xflix qa" -u abhishekmv2910 -d

# for running for a group of users
./run.py -c "COHORT_ME_QA_XFLIX_ENROLL_1714453952160" -l java -q "xflix qa" -u abhishekmv2910,shilpathakur2500 -d
```

### Using fastapi server on http://localhost:8000
```
export OAUTH2=<ACCESS_TOKEN>
pip install -r requirements.txt
uvicorn server:app
```

## Note
Reports are generated in reports folder in csv format

Use '-d' with run.py for debugging enabled

Java v21+ is required
