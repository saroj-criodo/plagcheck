import os
import sys
import argparse
import shutil
import subprocess
import tempfile
import csv
import logging
from datetime import datetime
from tabulate import tabulate

ANALYZED_REPO = ""

def show_help():
    help_message = """
    Usage: script.py -l <language> -i <input_dir> -o <output_dir> [-r <result_dir>] [-m <similarity_threshold>] [-b <stubs>] [-s]

    Arguments:
      -l <language>              Programming language to be analyzed
      -i <input_dir>             Directory containing the input files
      -o <output_dir>            Directory containing the output files
      -r <result_dir>            Optional directory to store the results (default: ./results)
      -m <similarity_threshold>  Optional similarity threshold (default: 0)
      -b <stubs>                 Optional stubs directory
      -s                         Use interactive selection for single file comparison

    Example:
      script.py -l java -i ./input -o ./output -r ./results -m 85 -b ./stubs
      script.py -l java -i ./input -o ./output -r ./results -s
    """
    print(help_message)

def beautify_csv(csv_file, formatted_file):
    if os.path.isfile(csv_file):
        logging.info("Beautifying the CSV output.")
        with open(csv_file, 'r') as infile:
            reader = csv.reader(infile)
            headers = next(reader)
            rows = list(reader)
            # Sort rows based on the similarity score (last column) in descending order
            rows = sorted(rows, key=lambda row: float(row[-1]), reverse=True)

        # Create a list of 'left' alignments for each column
        colalign = ["left"] * len(headers)
        
        table = tabulate(rows, headers, tablefmt='pretty', colalign=colalign)
        with open(formatted_file, 'w') as outfile:
            outfile.write("Formatted Results:\n")
            outfile.write(table)
        with open(formatted_file, 'r') as outfile:
            print(outfile.read())
    else:
        logging.info(f"CSV file not found: {csv_file}")

def run_jplag(language, input_file, output_file, result_dir, similarity_threshold, stubs, jar_path, combined_results):
    try:
        input_filename = os.path.basename(input_file)
        output_filename = os.path.basename(output_file)

        logging.info(f"Processing input file: {input_filename}")
        logging.info(f"Processing output file: {output_filename}")

        if input_filename != output_filename:
            global ANALYZED_REPO
            ANALYZED_REPO = input_filename

            temp_submissions_dir = tempfile.mkdtemp()
            shutil.copy(input_file, os.path.join(temp_submissions_dir, input_filename))
            shutil.copy(output_file, os.path.join(temp_submissions_dir, output_filename))

            cmd = [
                "java", "-jar", jar_path,
                "-l", language, "--csv-export", "-r", result_dir,
                "-m", str(similarity_threshold), "--overwrite"
            ]
            if stubs:
                cmd.extend(["-bc", stubs])
            cmd.append(temp_submissions_dir)

            logging.info(f"Running: {' '.join(cmd)}")
            result = subprocess.run(cmd, capture_output=True, text=True)

            if result.returncode == 0:
                logging.info(f"JPlag analysis for {input_filename} vs {output_filename} completed successfully.")
                results_csv_path = os.path.join(result_dir, "results.csv")
                logging.info(f"Looking for results CSV at: {results_csv_path}")
                
                if os.path.exists(results_csv_path):
                    with open(results_csv_path, 'r') as csvfile:
                        reader = csv.reader(csvfile)
                        headers = next(reader)
                        logging.info(f"CSV headers: {headers}")
                        row = next(reader)
                        logging.info(f"CSV row: {row}")
                        similarity = row[2]
                        with open(combined_results, 'a') as combined_csv:
                            writer = csv.writer(combined_csv)
                            writer.writerow([input_filename.split('.')[-2].replace(":","/"), output_filename.split('.')[-2].replace(":","/"), round(float(similarity) * 100, 2)])
                else:
                    logging.error(f"Results CSV file not found at {results_csv_path}")
            else:
                logging.error(f"Error: JPlag analysis for {input_filename} vs {output_filename} failed.")
                logging.error(result.stderr)

            shutil.rmtree(temp_submissions_dir)
    except Exception as e:
        logging.error(f"An error occurred in run_jplag: {str(e)}")
        raise

def check_fzf():
    try:
        subprocess.run(["fzf", "--version"], capture_output=True, text=True, check=True)
        return True
    except (subprocess.CalledProcessError, FileNotFoundError):
        return False

def interactive_selection(input_dir, output_dir):
    input_files = [os.path.join(input_dir, f) for f in os.listdir(input_dir)]
    output_files = [os.path.join(output_dir, f) for f in os.listdir(output_dir)]

    if len(input_files) == 1 and len(output_files) == 1:
        return input_files[0], output_files[0]

    if check_fzf():
        logging.info("Using fzf for interactive file selection.")
        input_file = subprocess.check_output(['fzf'], input='\n'.join(input_files).encode()).strip().decode()
        output_file = subprocess.check_output(['fzf'], input='\n'.join(output_files).encode()).strip().decode()
    else:
        logging.info("fzf not installed. Please install fzf for better interactive selection.")
        print("Available input files:")
        for i, f in enumerate(input_files):
            print(f"{i}: {f}")
        input_index = int(input("Select an input file by index: "))
        input_file = input_files[input_index]

        print("Available output files:")
        for i, f in enumerate(output_files):
            print(f"{i}: {f}")
        output_index = int(input("Select an output file by index: "))
        output_file = output_files[output_index]

    return input_file, output_file

if __name__ == "__main__":
    try:
        parser = argparse.ArgumentParser(description="JPlag analysis script.")
        parser.add_argument('-l', required=True, help="Programming language to be analyzed")
        parser.add_argument('-i', help="Directory containing the input files")
        parser.add_argument('-o', help="Directory containing the output files")
        parser.add_argument('-r', default="results", help="Directory to store the results")
        parser.add_argument('-m', type=int, default=0, help="Similarity threshold")
        parser.add_argument('-b', help="Stubs directory")
        parser.add_argument('-s', help="Use interactive selection for single file comparison")

        args = parser.parse_args()

        if not args.l or (not args.i and not args.s) or (not args.o and not args.s):
            logging.info(
                "Error: Language, input directory, and output directory are required unless using interactive single file mode.")
            show_help()
            sys.exit(1)

        if args.s and (not args.i or not args.o):
            logging.info("Error: Input and output directories are required for interactive single file selection.")
            show_help()
            sys.exit(1)

        logging.info("Starting JPlag analysis.")

        reports_dir = "reports"
        os.makedirs(reports_dir, exist_ok=True)

        jar_path = "./jplag-5.1.0-jar-with-dependencies.jar"

        if args.s:
            input_files = [os.path.join(args.i, f) for f in os.listdir(args.i)]
            output_files = [os.path.join(args.o, f) for f in os.listdir(args.o)]

            input_filename_without_ext = os.path.splitext(os.path.basename(args.s))[0]
            combined_results = os.path.join(reports_dir, f"{input_filename_without_ext}.csv")
            with open(combined_results, 'w') as f:
                writer = csv.writer(f)
                writer.writerow(["Original Repo", "Compared Repo", "Similarity(%)"])

            for output_file in output_files:
                run_jplag(args.l, args.s, output_file, args.r, args.m, args.b, jar_path, combined_results)
            for other_input_file in input_files:
                if args.s != other_input_file:
                    run_jplag(args.l, args.s, other_input_file, args.r, args.m, args.b, jar_path, combined_results)
            
            beautify_csv(combined_results, os.path.join(args.r, f"{input_filename_without_ext}_combined_results_formatted.txt"))
        else:
            input_files = [os.path.join(args.i, f) for f in os.listdir(args.i)]
            output_files = [os.path.join(args.o, f) for f in os.listdir(args.o)]

            for input_file in input_files:
                input_filename_without_ext = os.path.splitext(os.path.basename(input_file))[0]
                combined_results = os.path.join(reports_dir, f"{input_filename_without_ext}.csv")
                with open(combined_results, 'w') as f:
                    writer = csv.writer(f)
                    writer.writerow(["Original Repo", "Compared Repo", "Similarity(%)"])

                for output_file in output_files:
                    run_jplag(args.l, input_file, output_file, args.r, args.m, args.b, jar_path, combined_results)
                for other_input_file in input_files:
                    if input_file != other_input_file:
                        run_jplag(args.l, input_file, other_input_file, args.r, args.m, args.b, jar_path, combined_results)
                
                beautify_csv(combined_results, os.path.join(args.r, f"{input_filename_without_ext}_combined_results_formatted.txt"))

        logging.info("Script finished successfully.")
    except Exception as e:
        logging.error(f"An error occurred: {str(e)}")
        sys.exit(1)
