from fastapi import FastAPI, Form
from fastapi.responses import HTMLResponse
import subprocess
import os
import pandas as pd

app = FastAPI()

# Default values for the form
DEFAULT_COHORT = "COHORT_ME_QA_XFLIX_ENROLL_1714453952160"
DEFAULT_STUBS = "ME_QA_XFLIX_STUBS"
DEFAULT_LANGUAGE = "java"
DEFAULT_SEARCH_QUERY = "xflix qa"
DEFAULT_USER_LIST = "atharvabhandarkar96"


@app.get("/", response_class=HTMLResponse)
async def home():
    html_content = f"""
    <!DOCTYPE html>
    <html>
    <head>
        <title>Plagiarism Check</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <style>
            body {{
                padding: 20px;
            }}
            form {{
                max-width: 600px;
                margin: 0 auto;
            }}
            .form-group {{
                margin-bottom: 15px;
            }}
        </style>
        <script>
            function handleFormSubmit() {{
                const submitButton = document.getElementById("submit-button");
                submitButton.disabled = true;
                submitButton.textContent = "Processing...";
            }}
        </script>
    </head>
    <body>
        <div class="container">
            <h1 class="text-center">Run Plagiarism Check</h1>
            <form method="post" action="/run-plagcheck" onsubmit="handleFormSubmit()">
                <div class="form-group">
                    <label for="cohort">Cohort:</label>
                    <input type="text" class="form-control" id="cohort" name="cohort" value="{DEFAULT_COHORT}" required>
                </div>
                <div class="form-group">
                    <label for="stubs">Stubs (optional):</label>
                    <input type="text" class="form-control" id="stubs" name="stubs" value="{DEFAULT_STUBS}">
                </div>
                <div class="form-group">
                    <label for="language">Language:</label>
                    <input type="text" class="form-control" id="language" name="language" value="{DEFAULT_LANGUAGE}" required>
                </div>
                <div class="form-group">
                    <label for="search_query">Search Query:</label>
                    <input type="text" class="form-control" id="search_query" name="search_query" value="{DEFAULT_SEARCH_QUERY}" required>
                </div>
                <div class="form-group">
                    <label for="user_list">User List (optional):</label>
                    <input type="text" class="form-control" id="user_list" name="user_list" value="{DEFAULT_USER_LIST}">
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-primary" id="submit-button">Run Plagcheck</button>
                </div>
            </form>
        </div>
    </body>
    </html>
    """
    return HTMLResponse(content=html_content)


@app.post("/run-plagcheck", response_class=HTMLResponse)
async def run_plagcheck(cohort: str = Form(...), stubs: str = Form(""), language: str = Form(...),
                        search_query: str = Form(...), user_list: str = Form("")):
    # Construct the command to run
    command = [
        "python3", "run.py",
        "-c", cohort,
        "-l", language,
        "-q", search_query,
        "-d"
    ]

    if stubs:
        command.extend(["-s", stubs])

    if user_list:
        command.extend(["-u", user_list])

    # Cleanup reports
    subprocess.run(["rm", "-rf", "reports"])

    # Run the command
    process = subprocess.run(command, capture_output=True, text=True)

    if process.returncode == 0:
        # Concatenate all CSV files in the reports directory
        reports_dir = "./reports"
        csv_files = [os.path.join(reports_dir, f) for f in os.listdir(reports_dir) if f.endswith(".csv")]

        if csv_files:
            combined_df = pd.concat([pd.read_csv(f) for f in csv_files])
            combined_df_html = combined_df.to_html(index=False, classes='table table-striped', border=0)

            html_content = f"""
            <!DOCTYPE html>
            <html>
            <head>
                <title>Plagiarism Check Results</title>
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
                <style>
                    body {{
                        padding: 20px;
                    }}
                    table {{
                        width: 100%;
                        margin: 0 auto;
                        border-collapse: collapse;
                        font-size: 12px;
                    }}
                    th, td {{
                        padding: 12px;
                        text-align: left;
                        border-bottom: 1px solid #ddd;
                    }}
                    th {{
                        background-color: #f2f2f2;
                    }}
                </style>
            </head>
            <body>
                <div class="container">
                    <h1 class="text-center">Plagiarism Check Results</h1>
                    <div class="text-center mb-4">
                        <a href="/" class="btn btn-secondary">Back to Homepage</a>
                    </div>
                    {combined_df_html}
                </div>
            </body>
            </html>
            """
        else:
            html_content = "<h1>No CSV reports found</h1>"
    else:
        # On failure, show the error message
        html_content = f"""
        <!DOCTYPE html>
        <html>
        <head>
            <title>Error</title>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
            <style>
                body {{
                    padding: 20px;
                }}
            </style>
        </head>
        <body>
            <div class="container">
                <h1 class="text-center">Error</h1>
                <div class="text-center mb-4">
                    <a href="/" class="btn btn-secondary">Back to Homepage</a>
                </div>
                <pre>{process.stderr}</pre>
            </div>
        </body>
        </html>
        """

    return HTMLResponse(content=html_content, status_code=200)


@app.get("/view-reports", response_class=HTMLResponse)
async def view_reports():
    reports_dir = "./reports"
    if not os.path.exists(reports_dir):
        return HTMLResponse(content="<h1>No reports directory found</h1>", status_code=404)

    report_files = [f for f in os.listdir(reports_dir) if f.endswith('.csv')]
    if not report_files:
        return HTMLResponse(content="<h1>No reports found</h1>", status_code=404)

    report_links = "".join(
        f'<li><a href="/view-report/{report}">{report}</a></li>'
        for report in report_files
    )

    html_content = f"""
    <!DOCTYPE html>
    <html>
    <head>
        <title>Reports</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <style>
            body {{
                padding: 20px;
            }}
            h1 {{
                text-align: center;
                margin-bottom: 40px;
            }}
            ul {{
                list-style-type: none;
                padding: 0;
            }}
            li {{
                margin: 10px 0;
                font-size: 18px;
            }}
        </style>
    </head>
    <body>
        <div class="container">
            <h1>Available Reports</h1>
            <ul>
                {report_links}
            </ul>
            <div class="text-center mt-4">
                <a href="/" class="btn btn-secondary">Back to Homepage</a>
            </div>
        </div>
    </body>
    </html>
    """
    return HTMLResponse(content=html_content)
