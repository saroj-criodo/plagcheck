import os
import fnmatch
import logging
import argparse

# Set up logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

def sanitize_content(content):
    # Remove Unicode characters and illegal_chars
    illegal_chars = ['\\"', '\\']
    for char in illegal_chars:
        content = content.replace(char, "")
    sanitized_content = content.encode('ascii', 'ignore').decode('ascii')
    return sanitized_content

def handle_java_imports(lines):
    imports = set()
    non_import_content = ""
    for line in lines:
        if line.strip().startswith("import "):
            imports.add(line.strip())
        elif not line.strip().startswith("package "):
            non_import_content += line
    return imports, non_import_content

def handle_cpp_imports(lines):
    imports = set()
    non_import_content = ""
    for line in lines:
        if line.strip().startswith("#include"):
            imports.add(line.strip())
        else:
            non_import_content += line
    return imports, non_import_content

def handle_python_imports(lines):
    imports = set()
    non_import_content = ""
    for line in lines:
        if line.strip().startswith("import ") or line.strip().startswith("from "):
            imports.add(line.strip())
        else:
            non_import_content += line
    return imports, non_import_content

def handle_js_imports(lines):
    imports = set()
    non_import_content = ""
    for line in lines:
        if line.strip().startswith("import ") or line.strip().startswith("require("):
            imports.add(line.strip())
        else:
            non_import_content += line
    return imports, non_import_content

def handle_generic_imports(lines):
    # For other languages, treat all lines as non-import content
    non_import_content = "".join(lines)
    return set(), non_import_content

def get_import_handler(file_extension):
    if file_extension == ".java":
        return handle_java_imports
    elif file_extension == ".cpp" or file_extension == ".c":
        return handle_cpp_imports
    elif file_extension == ".py":
        return handle_python_imports
    elif file_extension == ".js":
        return handle_js_imports
    else:
        return handle_generic_imports

def aggregate_file_contents(file_paths):
    logging.info("Aggregating content from %d files.", len(file_paths))
    imports = set()
    non_import_content = ""
    
    for file_path in file_paths:
        logging.info(f"Reading file: {file_path}")
        file_extension = os.path.splitext(file_path)[1]
        import_handler = get_import_handler(file_extension)

        try:
            with open(file_path, 'r', encoding='utf-8', errors='ignore') as file:
                lines = file.readlines()
                file_imports, file_content = import_handler(lines)
                imports.update(file_imports)
                non_import_content += file_content
        except Exception as e:
            logging.error(f"Error reading file {file_path}: {e}")
    
    aggregated_content = "\n".join(sorted(imports)) + "\n\n" + non_import_content
    logging.info("Content aggregation complete.")
    return sanitize_content(aggregated_content)

def write_to_file(content, output_file):
    logging.info(f"Writing concatenated content to {output_file}")

    with open(output_file, 'w', encoding='utf-8', errors='ignore') as file:
        file.write(content)

    logging.info(f"Finished writing to {output_file}.")

def find_files(directory, patterns):
    logging.info("Searching for files in directory '%s' with patterns: %s", directory, patterns)
    found_files = []
    for root, _, files in os.walk(directory):
        for file in files:
            for pattern in patterns:
                if fnmatch.fnmatch(file, pattern):
                    found_files.append(os.path.join(root, file))
                    break
    logging.info("Found %d files", len(found_files))
    return found_files

def process_folders(folder, patterns, output_folder):
    # Create the output directory if it doesn't exist
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    # Get all subfolders (one level deep)
    subfolders = [f.path for f in os.scandir(folder) if f.is_dir()]

    for subfolder in subfolders:
        folder_name = os.path.basename(subfolder)
        output_file = os.path.join(output_folder, f"{folder_name}.java")

        file_paths = find_files(subfolder, patterns)
        concatenated_content = aggregate_file_contents(file_paths)
        if concatenated_content.strip():
            write_to_file(concatenated_content, output_file)
        else:
            print("No content to write to the file.")

def main():
    parser = argparse.ArgumentParser(description='Concatenate specified files in subfolders into a single file for each subfolder.')
    parser.add_argument('folder', help='The main folder containing subfolders with files.')
    parser.add_argument('patterns', help='Comma-separated file patterns to match, e.g., "*.java,*.cpp".')
    parser.add_argument('output_folder', help='Folder where the output files will be written.')

    args = parser.parse_args()

    # Split patterns by comma
    patterns = [pattern.strip() for pattern in args.patterns.split(',')]

    process_folders(args.folder, patterns, args.output_folder)

if __name__ == "__main__":
    main()

