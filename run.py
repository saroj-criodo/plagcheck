#!/usr/bin/env python

import os
import sys
import argparse
import subprocess
import logging
from datetime import datetime
from urllib.parse import urlparse

from repo_search.gitlab_crio import get_user_id_from_email, get_project_urls

# Define the supported languages and their file extensions
LANGUAGES = {
    "java": "*.java",
    "c": "*.c",
    "cpp": "*.cpp",
    "js": "*.js",
    "python": "*.py"
}


def setup_logging(debug_mode):
    os.makedirs('logs', exist_ok=True)
    timestamp = datetime.now().strftime("%Y%m%d_%H%M%S")
    logfile = f"logs/{timestamp}.log"

    logging.basicConfig(
        filename=logfile,
        filemode='a',
        format='%(asctime)s - %(levelname)s - %(message)s',
        level=logging.DEBUG if debug_mode else logging.INFO
    )

    console = logging.StreamHandler()
    console.setLevel(logging.DEBUG if debug_mode else logging.INFO)
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    console.setFormatter(formatter)
    logging.getLogger().addHandler(console)

    return logfile


def check_and_download_jplag():
    if not os.path.isfile("jplag-5.1.0-jar-with-dependencies.jar"):
        logging.info("jplag-5.1.0-jar-with-dependencies.jar not found. Downloading...")
        subprocess.run(
            ["wget", "https://github.com/jplag/JPlag/releases/download/v5.1.0/jplag-5.1.0-jar-with-dependencies.jar"],
            check=True)


def run_process(process, logs=False):
    if logs:
        # logging.info(f"Running: {process}")
        subprocess.run(process, check=True)
    else:
        subprocess.run(process, check=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)


def parse_repo_url(url):
    parsed_url = urlparse(url)
    repo_name = os.path.basename(parsed_url.path)
    owner_name = os.path.basename(os.path.dirname(parsed_url.path))
    return f"{owner_name}:{repo_name}"


def sanitize_repositories(source, extension, destination, logs):
    logging.info(f"Sanitizing repositories from {source} to {destination}")
    run_process(["python", "sanitize.py", source, extension, destination], logs)


def check_user_input(user_input, extension, cohort, debug):
    project_name = user_input
    if user_input.startswith("http"):
        project_name = user_input.split("/")[-1]
    elif "@" in user_input:
        project_name = get_user_id_from_email(user_input)
    logging.info(f"Checking for {project_name} in cohort: {cohort}")

    all_project_urls = get_project_urls(cohort)

    for project_url in all_project_urls:
        if project_name in project_url:
            sanitized_repo_path = check_repo(project_url, extension, cohort, debug)
            return sanitized_repo_path
    else:
        logging.info(f"No repo found for {project_name} in {cohort}")
        sys.exit(1)


def check_repo(url, extension, cohort, logs=False):
    repo_id = parse_repo_url(url)
    sanitized_repo_path = f"sanitized_output/user_repos/{cohort}/{repo_id}{extension}".replace("*", "")
    logging.info(f"Checking if already sanitized repository exists: {sanitized_repo_path}")

    if os.path.isfile(sanitized_repo_path):
        logging.info(f"Sanitized repository already exists: {sanitized_repo_path}")
        return sanitized_repo_path

    logging.info(f"Checking repository: {url}")

    return sanitized_repo_path


def download_user_repositories(cohort, logs):
    logging.info(f"Downloading user repositories for cohort: {cohort}")
    run_process(["python", "repo_search/repo_downloader.py", "gitlab_crio", cohort], logs)


def download_stub_repositories(stub, logs):
    logging.info(f"Downloading stub repositories: {stub}")
    run_process(["python", "repo_search/repo_downloader.py", "gitlab_crio", stub, "stub_repos"], logs)


def download_public_repositories(public, logs):
    logging.info(f"Downloading public repositories with search term: {public}")
    run_process(["python", "repo_search/repo_downloader.py", "github", public, "public_repos"], logs)


def run_plagiarism_check(language, cohort, stub, user=None, logs=False):
    if user:
        logging.info(f"Running plagiarism check for a single user: {user}")
        if stub:
            run_process([
                "python", "plagcheck.py", "-l", language,
                "-i", f"sanitized_output/user_repos/{cohort}",
                "-o", "sanitized_output/public_repos",
                "-b", f"sanitized_output/{stub}/stub_repos",
                "-s", user
            ], logs)
        else:
            run_process([
                "python", "plagcheck.py", "-l", language,
                "-i", f"sanitized_output/user_repos/{cohort}",
                "-o", "sanitized_output/public_repos",
                "-s", user
            ], logs)
    else:
        logging.info(f"Running plagiarism checks for all user repositories in cohort: {cohort}")
        if stub:
            run_process([
                "python", "plagcheck.py", "-l", language,
                "-i", f"sanitized_output/user_repos/{cohort}",
                "-o", "sanitized_output/public_repos",
                "-b", f"sanitized_output/{stub}/stub_repos"
            ], logs)
        else:
            run_process([
                "python", "plagcheck.py", "-l", language,
                "-i", f"sanitized_output/user_repos/{cohort}",
                "-o", "sanitized_output/public_repos"
            ], logs)


def cleanup(logs):
    logging.info("Cleanup")
    run_process(["rm", "-rf", "result", "result.zip"], logs)


def main():
    parser = argparse.ArgumentParser(description="A script to manage repository downloads and plagiarism checks.")
    parser.add_argument('-c', '--cohort', required=True, help='Cohort name to download user repositories')
    parser.add_argument('-s', '--stub', help='Stub name to download stub repositories [Optional]')
    parser.add_argument('-q', '--query', required=True, help='Search term for public repositories')
    parser.add_argument('-l', '--language', required=True, choices=LANGUAGES.keys(),
                        help='Programming language (java, c, cpp, js, python)')
    parser.add_argument('-u', '--users', help='List of user repository URL | username | user email, separated by commas for plagiarism check [Optional]')
    parser.add_argument('-d', '--debug', action='store_true', help='Enable debug mode with detailed logging')

    args = parser.parse_args()

    logfile = setup_logging(args.debug)

    if args.language not in LANGUAGES:
        parser.print_usage()
        sys.exit(1)

    check_and_download_jplag()
    extension = LANGUAGES[args.language]

    try:
        users = [args.users] if args.users and "," not in args.users else [user.strip() for user in args.users.split(",")] if args.users else [""]

        for user in users:
            user_sanitized_repo_path = check_user_input(user, extension, args.cohort, args.debug)
            if args.stub:
                download_stub_repositories(args.stub, args.debug)
                sanitize_repositories("stub_repos", extension, f"sanitized_output/{args.stub}/stub_repos", args.debug)

            download_user_repositories(args.cohort, args.debug)
            sanitize_repositories("user_repos", extension, f"sanitized_output/user_repos/{args.cohort}", args.debug)

            download_public_repositories(args.query, args.debug)
            sanitize_repositories("public_repos", extension, "sanitized_output/public_repos", args.debug)

            if not user == "":
                run_plagiarism_check(args.language, args.cohort, args.stub, user_sanitized_repo_path, args.debug)
            else:
                run_plagiarism_check(args.language, args.cohort, args.stub, logs=args.debug)
            cleanup(args.debug)

    except subprocess.CalledProcessError as e:
        logging.error(f"An error occurred: {e}")
        sys.exit(1)


if __name__ == "__main__":
    main()
