import os
import logging
import sys
import asyncio
import gitlab_crio
from aiohttp import ClientSession
from subprocess import run
from urllib.parse import urlparse, urlunparse
from repo_search import get_repositories, get_repository_urls

# Configure logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

# OAuth2 token for GitLab (Crio)
PRIVATE_TOKEN = os.getenv('OAUTH2')
if not PRIVATE_TOKEN:
    logging.error("OAuth token not found in environment variables.")
    sys.exit(1)

async def download_repo(url, session, download_path, platform):
    # Extract the username from the URL
    parsed_url = urlparse(url)
    path_parts = parsed_url.path.strip('/').split('/')
    
    if len(path_parts) < 2:
        logging.error(f'Invalid URL: {url}')
        return
    
    username = path_parts[0]
    repo_name = path_parts[1]

    clone_path = os.path.join(download_path, f"{username}:{repo_name}".rstrip('-'))

    if os.path.exists(clone_path):
        logging.info(f'Repository {repo_name} already exists at {clone_path}. Skipping download.')
        return

    if platform == 'gitlab_crio':
        # Form URL with OAuth2 token for GitLab (Crio)
        netloc = f"oauth2:{PRIVATE_TOKEN}@{parsed_url.netloc}"
        url_with_token = urlunparse((parsed_url.scheme, netloc, parsed_url.path, '', '', ''))
        clone_command = f'git clone --depth=1 {url_with_token} {clone_path}'
    else:
        clone_command = f'git clone --depth=1 {url} {clone_path}'

    logging.info(f'Starting download of {url}')

    process = await asyncio.create_subprocess_shell(
        clone_command,
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE
    )

    stdout, stderr = await process.communicate()

    if process.returncode == 0:
        logging.info(f'Successfully downloaded {url} to {clone_path}')
    else:
        logging.error(f'Failed to download {url}. Error: {stderr.decode()}')

def get_gitlab_crio_repos_from_group(group_name):
    return gitlab_crio.get_project_urls(group_name)

async def download_repos(urls, download_path,platform):
    async with ClientSession() as session:
        tasks = [download_repo(url, session, download_path,platform) for url in urls]
        return await asyncio.gather(*tasks)

def main(platform, repo_name, download_path="public", results_limit=None):
    if platform == 'gitlab_crio':
        urls = get_gitlab_crio_repos_from_group(repo_name)
    else:
        repositories = get_repositories(platform, repo_name, results_limit=results_limit)
        urls = get_repository_urls(platform, repositories)
    
    if not os.path.exists(download_path):
        os.makedirs(download_path)
    
    if results_limit:
        urls = urls[:results_limit]
    
    logging.info(f'Starting to download {len(urls)} repositories to {download_path}')
    
    asyncio.run(download_repos(urls, download_path,platform))

if __name__ == '__main__':
    if len(sys.argv) < 3 or len(sys.argv) > 5:
        logging.error("Usage: python repo_downloader.py <platform> <repo_name> [download_path|results_limit] [results_limit|download_path]")
        logging.error("Platform should be either 'github' or 'gitlab' or 'gitlab_crio'")
        sys.exit(1)
    
    global platform
    platform = sys.argv[1].lower()
    repo_name = sys.argv[2]
    if platform == 'gitlab_crio':
        download_path = "user_repos"
    else:
        download_path = "public_repos"
    results_limit = None
    
    # Process optional arguments
    for arg in sys.argv[3:]:
        if arg.isdigit():
            results_limit = int(arg)
        else:
            download_path = arg
    
    if platform not in ['github', 'gitlab', 'gitlab_crio']:
        logging.error("Platform should be either 'github' or 'gitlab' or 'gitlab_crio'")
        sys.exit(1)
    
    main(platform, repo_name, download_path, results_limit)

