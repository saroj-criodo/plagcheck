import logging
import requests
import random
import time

# Configure logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

user_agent_list = [
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.1 Safari/605.1.15',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:77.0) Gecko/20100101 Firefox/77.0',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36',
]

GITHUB_API_URL = 'https://api.github.com/search/repositories'
GITLAB_API_URL = 'https://gitlab.com/api/v4/projects'

def search_github_repositories(query, page, headers):
    params = {
        'q': query,
        'sort': 'updated',
        'order': 'desc',
        'per_page': 100,
        'page': page
    }
    logging.info(f'Searching GitHub repositories: {query}, page: {page}')
    response = requests.get(GITHUB_API_URL, headers=headers, params=params)
    response.raise_for_status()
    return response.json()

def search_gitlab_repositories(query, page, headers):
    params = {
        'search': query,
        'simple': True,
        'per_page': 100,
        'page': page
    }
    logging.info(f'Searching GitLab repositories: {query}, page: {page}')
    response = requests.get(GITLAB_API_URL, headers=headers, params=params)
    response.raise_for_status()
    return response.json()

def get_repositories(platform, repo_name, results_limit=None, sleep_time=5):
    query = f'{repo_name} in:name'
    page = 1
    all_repositories = []

    while True:
        user_agent = random.choice(user_agent_list)
        headers = {'User-Agent': user_agent}
        logging.info(f'Using User-Agent: {user_agent}')
        
        if platform == 'github':
            result = search_github_repositories(query, page, headers)
            repositories = result.get('items', [])
        elif platform == 'gitlab':
            result = search_gitlab_repositories(repo_name, page, headers)
            repositories = result
        
        if not repositories:
            logging.info(f'No more repositories found on {platform} for query "{repo_name}" on page {page}.')
            break
        
        all_repositories.extend(repositories)
        logging.info(f'Found {len(repositories)} repositories on page {page}.')
        
        if results_limit and len(all_repositories) >= results_limit:
            all_repositories = all_repositories[:results_limit]
            logging.info(f'Reached results limit of {results_limit}. Stopping search.')
            break
        
        page += 1
        
        if len(repositories) == 100:
            # Sleep to respect rate limits
            logging.info(f'Sleeping for {sleep_time} seconds to respect rate limits.')
            time.sleep(sleep_time)  # Adjust the sleep time as necessary to avoid rate limiting
    
    logging.info(f'Total repositories found: {len(all_repositories)}')
    return all_repositories

def get_repository_urls(platform, repositories):
    urls = []
    for repo in repositories:
        if platform == 'github':
            urls.append(repo['html_url'])
        elif platform == 'gitlab':
            urls.append(repo['web_url'])
    return urls

def log_repositories(platform, repositories, results_limit=None):
    urls = get_repository_urls(platform, repositories)
    if results_limit:
        urls = urls[:results_limit]
    logging.info(f'Repositories found on {platform.capitalize()}:')
    for url in urls:
        logging.info(f"Repository URL: {url}")

if __name__ == '__main__':
    import sys
    if len(sys.argv) not in [3, 4]:
        logging.error("Usage: python repo_search.py <platform> <repo_name> [results_limit]")
        logging.error("Platform should be either 'github' or 'gitlab'")
        sys.exit(1)
    
    platform = sys.argv[1].lower()
    repo_name = sys.argv[2]
    results_limit = int(sys.argv[3]) if len(sys.argv) == 4 else None
    
    if platform not in ['github', 'gitlab']:
        logging.error("Platform should be either 'github' or 'gitlab'")
        sys.exit(1)
    
    repositories = get_repositories(platform, repo_name, results_limit=results_limit)
    log_repositories(platform, repositories, results_limit=results_limit)

