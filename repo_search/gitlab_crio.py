import os
import requests
import logging
import argparse

# Configuration
PRIVATE_TOKEN = os.getenv('OAUTH2')
GITLAB_HOST = "https://gitlab.crio.do"
PER_PAGE = 100

# Validate token
if not PRIVATE_TOKEN:
    logging.error("OAuth token not found in environment variables.")
    exit(1)

# Setup logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')


# Get Group ID
def get_cohort_id(cohort):
    url = f"{GITLAB_HOST}/api/v4/groups?search={cohort}"
    headers = {"PRIVATE-TOKEN": PRIVATE_TOKEN}
    logging.debug(f"Fetching group ID from URL: {url} with headers: {headers}")
    response = requests.get(url, headers=headers)
    logging.debug(f"Response status code: {response.status_code}")
    logging.debug(f"Response text: {response.text}")
    response.raise_for_status()
    groups = response.json()
    logging.debug(f"Groups JSON: {groups}")
    if not groups:
        return None
    return groups[0]['id']


def get_user_id_from_email(email):
    url = f"{GITLAB_HOST}/api/v4/users?search={email}"
    headers = {"PRIVATE-TOKEN": PRIVATE_TOKEN}
    logging.debug(f"Fetching user ID from URL: {url} with headers: {headers}")
    response = requests.get(url, headers=headers)
    logging.debug(f"Response status code: {response.status_code}")
    logging.debug(f"Response text: {response.text}")
    response.raise_for_status()
    users = response.json()
    if not users:
        return None
    return users[0]['username']


# Fetch projects
def fetch_projects(url):
    headers = {"PRIVATE-TOKEN": PRIVATE_TOKEN}
    logging.debug(f"Fetching projects from URL: {url} with headers: {headers}")
    response = requests.get(url, headers=headers)
    logging.debug(f"Response status code: {response.status_code}")
    logging.debug(f"Response text: {response.text}")
    response.raise_for_status()
    return response.json()


# Get total number of pages
def get_total_pages(url):
    headers = {"PRIVATE-TOKEN": PRIVATE_TOKEN}
    logging.debug(f"Fetching total pages from URL: {url} with headers: {headers}")
    response = requests.get(url, headers=headers)
    logging.debug(f"Response status code: {response.status_code}")
    logging.debug(f"Response headers: {response.headers}")
    response.raise_for_status()
    total_pages = response.headers.get('X-Total-Pages', 1)
    return int(total_pages)


def get_project_urls(cohort):
    cohort_id = get_cohort_id(cohort)
    if not cohort_id:
        logging.error("Group not found")
        return []

    logging.info(f"Group ID: {cohort_id}")

    url = f"{GITLAB_HOST}/api/v4/groups/{cohort_id}/projects?per_page={PER_PAGE}"
    total_pages = get_total_pages(url)

    logging.info(f"Total pages: {total_pages}")

    project_urls = []
    for page in range(1, total_pages + 1):
        page_url = f"{url}&page={page}"
        projects = fetch_projects(page_url)
        for project in projects:
            project_urls.append(project['web_url'])

    return project_urls


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Fetch project URLs from a GitLab group.',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument(
        'cohort',
        type=str,
        help='Name of the GitLab group to search for'
    )

    args = parser.parse_args()
    project_urls = get_project_urls(args.cohort)
    for url in project_urls:
        print(url)
